# pull official base image
FROM python:3.10

# set work directory
WORKDIR ./pdf_info

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV REPLICATE_API_TOKEN=r8_eakbchnDACLR2n79ngQWDhgUpdeOrg42j7QfH

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt
#CMD export REPLICATE_API_TOKEN=r8_eakbchnDACLR2n79ngQWDhgUpdeOrg42j7QfH

# copy project
COPY . .

CMD gunicorn pdf_information_project.wsgi:application --bind 0.0.0.0:8008 --timeout 900 --workers=3 --threads=3

