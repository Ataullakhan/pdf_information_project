from django.apps import AppConfig


class PdfInformationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pdf_information'
