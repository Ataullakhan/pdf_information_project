from pydantic import BaseModel


class TableColumn(BaseModel):
    """Table column."""

    name: str
    dtype: str | None


class ForeignKey(BaseModel):
    """Foreign key."""

    # Referenced column
    column: TableColumn
    # References table name
    references_name: str
    # References column
    references_column: TableColumn


class Table(BaseModel):
    """Table."""

    name: str
    columns: list[TableColumn] | None
    pks: list[TableColumn] | None
    # FK from this table to another column in another table
    fks: list[ForeignKey] | None


class PromptFormatter:
    """PromptFormatter class.

    From https://arxiv.org/pdf/2204.00498.pdf.
    """

    table_sep: str = "\n\n"

    def __init__(self, tables: list[Table]) -> None:
        self.tables = tables
        # self.table_str = self.format_tables(tables)

    def format_table(self, table: Table) -> str:
        """Get table format."""
        table_fmt = []
        table_name = table.name
        for col in table.columns or []:
            # This is technically an incorrect type, but it should be a catchall word
            table_fmt.append(f"    {col.name} {col.dtype or 'any'}")
        if table.pks:
            table_fmt.append(
                f"    primary key ({', '.join(pk.name for pk in table.pks)})"
            )
        for fk in table.fks or []:
            table_fmt.append(
                f"    foreign key ({fk.column.name}) references {fk.references_name}({fk.references_column.name})"  # noqa: E501
            )
        if table_fmt:
            all_cols = ",\n".join(table_fmt)
            create_tbl = f"CREATE TABLE {table_name} (\n{all_cols}\n)"
        else:
            create_tbl = f"CREATE TABLE {table_name}"
        return create_tbl

    def format_tables(self, tables: list[Table]) -> str:
        """Get tables format."""
        return self.table_sep.join(self.format_table(table) for table in tables)

    def format_prompt(
        self,
        instruction: str,
    ) -> str:
        """Get prompt format."""
        sql_prefix = "SELECT"
        # return f"""{self.table_str}\n\n\nUsing valid SQLite,
        # act as a Text-to-SQL Query Generator.
        # Convert the following natural language request into a SQL query by referencing the provided tables.
            #  Craft a succinct SQL query while maintaining simplicity.
        #   Only one SQL query is required with No explanation and extra text. \n\n-- Question: {instruction}\nAnswer: """  # noqa: E501

        table_str = """-- descriptive.attrition_view_v2 source

    CREATE TABLE descriptive.attrition_view_v2 (
        employee_id int4,
        employee_name   varchar(255),
        employment_status   varchar(255),
        last_working_daydate,
        month   int4,
        quarter_nametext,
        fin_yeartext,
        resignation_approval_date   date,
        confirmation_status varchar(255),
        actin_type  varchar(255),
        action_reason   varchar(255),
        repmanager_id   int4,
        repmanager_name varchar(255),
        gender  varchar(255),
        grade   varchar(255),
        trans_month date,
        emp_age float8  ,
        buhead_id   int4,
        buhead_name varchar(255),
        hod_id  int4,
        hod_namevarchar(255),
        education   varchar(255),
        joining_datedate,
        dept_clustervarchar(255),
        hrbpvarchar(255),
        attrition_type  varchar(255),
        country varchar(255),
        company_codeint4,
        company varchar(255),
        company_infovarchar(255),
        sub_company_infovarchar(255),
        business_unit_code  int4,
        business_unit   varchar(255),
        sub_business_unit_code  int4,
        sub_business_unit   varchar(255),
        division_code   int4,
        divisionvarchar(255),
        sub_division_code   int4,
        sub_divisionvarchar(255),
        department_code int4,
        department  varchar(255),
        hiring_channel  varchar(255),
        level_band  varchar(255),
        job_rolevarchar(255),
        management_category varchar(255),
        resignation_datedate,
        emp_tenure  int4,
        emp_tenure_in_monthsfloat8  ,
        last_date_of_promotion  date,
        rating_labelvarchar(255),
        training_data   text,
        promotion_data  text,
        employee_category   text)"""

        return f"""{table_str}\n\n-- Using valid SQLite, answer the following questions for the tables provided above.\n
        -- Where Schema name is descriptive\n
        -- Instructions: In your response, make sure to use the schema-qualified table name \n
        for example: descriptive.tablename when referring to the table.\n
        Question: {instruction}\nAnswer: """

    def format_model_output(self, output_sql: str) -> str:
        """Format model output.

        Our prompt ends with SELECT so we need to add it back.
        """
        if not output_sql.lower().startswith("select"):
            output_sql = "SELECT " + output_sql.strip()
        return output_sql
