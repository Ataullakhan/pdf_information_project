# import requests
import docx2txt
import PyPDF2
import re
import replicate
import time
import os


def convert_to_text_content(file_path, file_extension):
    text = ''
    if file_extension == 'docx':
        # docx file to normal text
        text = docx2txt.process(file_path)
    elif file_extension == 'pdf':
        # PDF file to normal text
        with open(file_path, 'rb') as file:
            pdf_reader = PyPDF2.PdfReader(file)
            for page in pdf_reader.pages:
                text += page.extract_text()
    return text


def clean_text(text):
    # # Remove unnecessary punctuation and symbols
    # cleaned_text = re.sub(r'[^\w\s]', '', text)

    # Normalize whitespace
    cleaned_text = re.sub(r'\s+', ' ', text)

    # Convert to lowercase
    # cleaned_text = cleaned_text.lower()

    return cleaned_text


# Initialize debounce variables
last_call_time = 0
debounce_interval = 2  # Set the debounce interval (in seconds) to your desired value


def debounce_replicate_run(llm, prompt, max_len, temperature, top_p, API_TOKEN):
    global last_call_time
    print("last call time: ", last_call_time)

    # Get the current time
    current_time = time.time()

    # Calculate the time elapsed since the last call
    elapsed_time = current_time - last_call_time

    # Check if the elapsed time is less than the debounce interval
    if elapsed_time < debounce_interval:
        print("Debouncing")
        return "Hello! You are sending requests too fast. Please wait a few seconds before sending another request."

    # Update the last call time to the current time
    last_call_time = time.time()

    output = replicate.run(llm,
                           input={"prompt": prompt, "max_length": max_len, "temperature": temperature,
                                  "top_p": top_p, "repetition_penalty": 1}, api_token=API_TOKEN)
    return output


def extract_sql_query(text):
    # Define the pattern to match the SQL query
    pattern = r'```sql(.*?)```'

    # Search for the pattern in the text
    match = re.search(pattern, text, re.DOTALL)

    if match:
        # Extract and return the matched SQL query
        sql_query = match.group(1).strip()
        return sql_query
    else:
        return None






