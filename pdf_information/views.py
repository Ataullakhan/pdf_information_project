import io
from django.shortcuts import render
from django.core.files.storage import FileSystemStorage
# from django.http import HttpResponse
from django.conf import settings
import os
# import textract
# from transformers import GPT2TokenizerFast
# from langchain.text_splitter import RecursiveCharacterTextSplitter
# from langchain.embeddings import OpenAIEmbeddings
# from langchain.vectorstores import FAISS
# from langchain.llms import OpenAI
# from langchain.chains import ConversationalRetrievalChain
import requests
from django.http import JsonResponse
from django.shortcuts import render
# from db_connectors import PostgresConnector
# from prompt_formatters import RajkumarFormatter
from transformers import AutoModelForCausalLM, AutoTokenizer

from pdf_information.db_connectors import PostgresConnector
from pdf_information.prompt_formatters import PromptFormatter
import json
from bardapi import Bard
from django.views.decorators.csrf import csrf_exempt

path = settings.MEDIA_ROOT

from pdf_information.utils import convert_to_text_content, clean_text, debounce_replicate_run, extract_sql_query


# def index(request):
#     if request.method == 'POST':
#
#         dir_name = path + "/"
#         test = os.listdir(dir_name)
#         for item in test:
#             if item.endswith(".pdf"):
#                 os.remove(os.path.join(dir_name, item))
#
#
#         pdf_file = request.FILES['pdf_file']
#         category = request.POST.get('category', '')
#         fs = FileSystemStorage()
#         filename = fs.save(pdf_file.name, pdf_file)
#
#         files = path + '/' + filename
#
#         print('1111111111', filename)
#         print('2222222222', category)
#
#         url = "http://139.59.88.34:85/api/predict/"
#         project_id = 1
#
#         file_extension = filename.split('/')[-1].split('.')[-1]
#         file_content = convert_to_text_content(files, file_extension)
#
#         cleaned_text = clean_text(file_content)
#
#         # question = request.POST.get('question', '')
#
#         if category == "patient_chart":
#             default_questions = ["Summarize text with key highlights?", "What are the Patient Details?"]
#         elif category == "project_document":
#             default_questions = ["What is the name of the file?", "When is the last modified date ?",
#                                  "What is the version ?", "What is the status of the document?"]
#         elif category == "claim_form":
#             default_questions = ["Claim Details", "Required Documents"]
#         elif category == "invoice":
#             default_questions = ["Invoice Summary", "Payment Details"]
#         else:
#             default_questions = []
#
#         data = [
#             {
#                 "file_content": cleaned_text,
#                 "questions": default_questions,
#                 "project_id": project_id,
#                 "model_type": 'large'
#             }
#         ]
#
#         response = requests.post(url, json=data)
#
#         print(response)
#
#         data = response.json()
#         print('data----------', data)
#
#         data['filename'] = filename
#
#
#         return JsonResponse(data)
#
#     return render(request, 'base.html')


REPLICATE_API_TOKEN = 'r8_eakbchnDACLR2n79ngQWDhgUpdeOrg42j7QfH'
# Your your (Replicate) models' endpoints:
REPLICATE_MODEL_ENDPOINT7B = 'a16z-infra/llama7b-v2-chat:4f0a4744c7295c024a1de15e1a63c880d3da035fa1f49bfd344fe076074c8eea'
REPLICATE_MODEL_ENDPOINT13B = 'a16z-infra/llama13b-v2-chat:df7690f1994d94e96ad9d568eac121aecf50684a0b0963b25a41cc40061269e5'
REPLICATE_MODEL_ENDPOINT70B = 'replicate/llama70b-v2-chat:e951f18578850b652510200860fc4ea62b3b16fac280f83ff32282f87bbd2e48'

llm = REPLICATE_MODEL_ENDPOINT13B
max_len = 4096
temperature = 0.5
top_p = 0.9

API_TOKEN = REPLICATE_API_TOKEN

@csrf_exempt
def index(request):
    if request.method == 'POST':

        dir_name = path + "/"
        test = os.listdir(dir_name)
        for item in test:
            if item.endswith(".pdf"):
                os.remove(os.path.join(dir_name, item))

        pdf_file = request.FILES['pdf_file']
        category = request.POST.get('category', '')
        fs = FileSystemStorage()
        filename = fs.save(pdf_file.name, pdf_file)

        files = path + '/' + filename
        fileurl = fs.url(filename)

        print('1111111111', filename)
        print('2222222222', category)

        file_extension = filename.split('/')[-1].split('.')[-1]
        file_content = convert_to_text_content(files, file_extension)

        cleaned_text = clean_text(file_content)
        PRE_PROMPT = ""
        if category == "patient_chart":
            default_questions = ["What are the Patient Details?",
                                 "Can you identify any disease mentioned in the Context,"
                                 " and if so, what is its corresponding ICD code?"]
            PRE_PROMPT = ("You are a helpful assistant. Act as a Clinical Document Expert."
                          " give information in bullet points. You only respond once as Assistant.")

        elif category == "project_document":
            default_questions = ["What is the name of the Document Mention in Context ?",
                                 "When is the last modified date Mention in Context ?",
                                 "What is the Version Mention in Context ?",
                                 "What is the Status Mention in Context ?"]
            PRE_PROMPT = "You are a helpful assistant. You only respond once as Assistant."

        elif category == "claim_form":
            default_questions = ["Please provide the Claimant's details or "
                                 "Claim Details as mentioned in the claim form context."]
            PRE_PROMPT = ("You are a helpful assistant. Act as a Claim Document Expert or Claims Specialist."
                          " give information in bullet points. You only respond once as Assistant.")

        elif category == "invoice":
            default_questions = ["Get Invoice information like Invoice number, Invoice date, Due date,"
                                 " Billing address, Payment Details etc. from the Context"]
            PRE_PROMPT = ("You are a helpful assistant. Act as a Invoice Document Expert. "
                          "give information in bullet points. You only respond once as Assistant.")
            
        else:
            default_questions = []

        f_result = []
        for prompt in default_questions:
            dd = {"question": prompt}

            string_dialogue = PRE_PROMPT + "\n\nContext:" + cleaned_text + "\n\n Question: " + prompt + "\nAnswer:\n"

            output = debounce_replicate_run(llm, string_dialogue, max_len, temperature, top_p, API_TOKEN)

            full_response = ''
            for item in output:
                full_response += item


            print('-------------------------------------------------')
            print('+++++++++', full_response)

            dd['answer'] = full_response
            f_result.append(dd)

        data = {"results": f_result, 'filename': fileurl}

        return JsonResponse(data)

    return render(request, 'base.html')

@csrf_exempt
def sql_query_generator(request):
    if request.method == 'POST':
        # Retrieve form data
        # user = request.POST.get('user')
        # password = request.POST.get('password')
        # dbname = request.POST.get('dbname')
        # host = request.POST.get('host')
        # port = request.POST.get('port')

        user = 'biocon-user'
        password = 'biocon-user'
        dbname = 'biocon-database'
        host = '167.71.226.35'
        port = 5432
        query = request.POST.get('query')

        # Perform SQL query generation
        postgres_connector = PostgresConnector(user=user, password=password, dbname=dbname, host=host, port=port)
        postgres_connector.connect()
        db_schema = [postgres_connector.get_schema(table) for table in postgres_connector.get_tables()]
        formatter = PromptFormatter(db_schema)

        # model_name = "NumbersStation/nsql-2B"
        # tokenizer = AutoTokenizer.from_pretrained(model_name)
        # model = AutoModelForCausalLM.from_pretrained(model_name)
        # # #
        # generated_sql = get_sql(query, formatter, tokenizer, model)
        # tokenizer, model
        generated_sql = get_sql(query, formatter)



        # ##### using llama-2 sql ---------------------- generation###########

        print(generated_sql)

        llm = "gregwdata/defog-sqlcoder-q8:ad5a6671d23f7eeb47fde0b6e1ced085bdb14854bc1494ec3b96dfa2ed993a7a"

        output = debounce_replicate_run(llm, generated_sql, max_len, temperature, top_p, API_TOKEN)

        full_response = ''
        for item in output:
            full_response += item

        print('-----------------', full_response.strip())
        # # #############################################################################3

        # ########### Using Bard ---- ##############################
        # print(generated_sql)
        # #
        # bard = Bard(token_from_browser=True)
        # res = bard.get_answer(generated_sql)
        # print('------------', res['content'])
        # ###########################################################

        ###################################################################

        # generated_sql = generated_sql.lower().split('answer')[-1]
        # generated_sql = generated_sql.replace('\n', '').strip()
        # generated_sql = generated_sql.replace(':', '').strip()
        #
        # print('1111', generated_sql)

        # print('22222', type(res))

        # sql_query = extract_sql_query(res['content'])
        sql_query = full_response.strip()
        if sql_query:
            res = postgres_connector.run_sql_as_df(sql_query)
        else:
            if 'employee' in query:
                res = postgres_connector.run_sql_as_df('select * from employees')
            elif 'companie' in query:
                res = postgres_connector.run_sql_as_df('select * from companies')
            else:
                res = postgres_connector.run_sql_as_df('select * from employees')

        #########################

        out = res.to_json(orient='records')

        # print('json_output', out)

        parsed_data = json.loads(out)
        transformed_data = []

        # Iterate through the original data and convert it into the desired format
        for item in parsed_data:
            if len(item) > 2:
                key = list(item.values())[1]
                value = list(item.values())[-1]
                transformed_data.append({"key": key, "value": value})
            elif len(item) == 2:
                key = list(item.values())[0]
                value = list(item.values())[-1]
                transformed_data.append({"key": key, "value": value})
            elif len(item) < 2:
                key = list(item.keys())[0]
                value = list(item.values())[0]
                transformed_data.append({"key": key, "value": value})

        # Convert the transformed data back to JSON
        # transformed_json = json.dumps(transformed_data)

        # print('--------------', transformed_json)

        ###########################

        str_io = io.StringIO()
        res.to_html(buf=str_io, classes='table table-striped projectSpreadsheet')
        table_df_html_str = str_io.getvalue()

        return render(request, 'sql_query_generator.html', {'sql_result': table_df_html_str, 'json_res': transformed_data})
    else:
        return render(request, 'sql_query_generator.html', {'sql_result': None, 'json_res': None})


def get_sql(instruction, formatter):
    # tokenizer, model
    prompt = formatter.format_prompt(instruction)

    # print('prompt---', prompt)
    # inputs = tokenizer.encode(prompt, return_tensors="pt")
    # outputs = model.generate(inputs, max_length=4098, do_sample=True)
    # generated_sql = tokenizer.decode(outputs[0], skip_special_tokens=True)
    # print('generated_sqlsssssssssss', generated_sql)
    # return formatter.format_model_output(generated_sql)
    return prompt

@csrf_exempt
def contract_draft(request):
    if request.method == 'POST':

        dir_name = path + "/"
        test = os.listdir(dir_name)
        for item in test:
            if item.endswith(".pdf"):
                os.remove(os.path.join(dir_name, item))

        pdf_file = request.FILES['pdf_file']
        category = request.POST.get('questionInput', '')
        fs = FileSystemStorage()
        filename = fs.save(pdf_file.name, pdf_file)

        fileurl = fs.url(filename)

        files = dir_name + filename

        file_extension = filename.split('/')[-1].split('.')[-1]
        file_content = convert_to_text_content(files, file_extension)

        cleaned_text = clean_text(file_content)
        PRE_PROMPT = ""
        if category == "Summery":
            default_questions = ["Could you please provide the summary of the contract? and Can you give me a brief "
                                 "overview of the key terms and conditions in the contract?"]
            PRE_PROMPT = "You are a helpful assistant. Act as a legal contract Document Expert" \
                         " give information in bullet points." \
                         " You only respond once as Assistant."

        elif category == "Reviewing Contract Structure":
            default_questions = ["Analyze the organization and structure of the contract."
                                 " Are the sections and clauses logically arranged? "
                                 "Is the contract easy to navigate and understand?"]
            PRE_PROMPT = "You are a helpful assistant. Act as a legal contract Document Expert" \
                         " give information in bullet points. " \
                         "You only respond once as Assistant."
        elif category == "Identifying Parties and Signatories":
            default_questions = ["Identify the involved parties and ensure that all necessary individuals"
                                 " or entities are listed correctly. "
                                 "Check if authorized signatories have signed the contract."]
            PRE_PROMPT = "You are a helpful assistant. Act as a legal contract Document Expert." \
                         " give information in bullet points. You only respond once as Assistant."
        elif category == "Assessing Contract Scope":
            default_questions = ["Determine the scope of the contract. "
                                 "Does it clearly define the goods, services, or obligations involved? "
                                 "Are there any ambiguous terms that need clarification?"]
            PRE_PROMPT = "You are a helpful assistant. Act as a legal contract Document Expert." \
                         " give information in bullet points. You only respond once as Assistant."
        elif category == "Examining Payment Terms":
            default_questions = ["Review the payment terms, including the amount, schedule, and methods of payment."
                                 " Ensure that all financial aspects are well-defined and reasonable."]
            PRE_PROMPT = "You are a helpful assistant. Act as a legal contract Document Expert." \
                         " give information in bullet points. You only respond once as Assistant."
        elif category == "Verifying Duration and Termination":
            default_questions = ["Check the duration of the contract and the conditions for termination. "
                                 "Look for any automatic renewal clauses and"
                                 " assess whether termination conditions are fair and balanced."]
            PRE_PROMPT = "You are a helpful assistant. Act as a legal contract Document Expert." \
                         " give information in bullet points. You only respond once as Assistant."
        else:
            default_questions = [category]
            PRE_PROMPT = "You are a helpful assistant. Act as a legal contract Document Expert." \
                         " give information in bullet points. You only respond once as Assistant."

        f_result = []
        for prompt in default_questions:
            dd = {"question": prompt}
            string_dialogue = PRE_PROMPT + "\n\nContext:" + cleaned_text + "\n\n Question: " + prompt + "\nAnswer:\n"

            output = debounce_replicate_run(llm, string_dialogue, max_len, temperature, top_p, API_TOKEN)

            full_response = ''
            for item in output:
                full_response += item

            dd['answer'] = full_response
            f_result.append(dd)

        data = {"results": f_result, 'filename': fileurl}

        return JsonResponse(data)

    return render(request, 'contract_page.html')


